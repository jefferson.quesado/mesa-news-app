import 'package:flutter_test/flutter_test.dart';
import 'package:mesa_news/model/noticia.dart';

import 'util/pseudobanco.dart';

void main() {
  final String marmotaUrl = "https://marmota.dev";
  final String outraUrl = "https://outra.dev";
  test("strings de teste são distintas", () => expect(marmotaUrl, isNot(equals(outraUrl))));
  group("testando o pseudobanco", () {
    test("pseudobanco reconhece que não salvou URL", () async {
      var favQueryfier = MapQueryfier();
      var favoritou = await favQueryfier.favorito(marmotaUrl);
      expect(favoritou, equals(false));
    });
    test("pseudobanco armazena URL", () async {
      var favQueryfier = MapQueryfier();
      await favQueryfier.marcarFavorito(marmotaUrl, true);
      var favoritou = await favQueryfier.favorito(marmotaUrl);
      expect(favoritou, equals(true));
    });
    test("pseudobanco reconhece toggle", () async {
      var favQueryfier = MapQueryfier();
      await favQueryfier.toggleFavorito(marmotaUrl);
      var favoritou = await favQueryfier.favorito(marmotaUrl);
      expect(favoritou, equals(true));
    });
    test("pseudobanco reconhece URL da construção", () async {
      var favQueryfier = MapQueryfier(urlFavs: {marmotaUrl});
      var favoritou = await favQueryfier.favorito(marmotaUrl);
      expect(favoritou, equals(true));
    });
    test("pseudobanco reconhece desfavoritar sobre URL da construção", () async {
      var favQueryfier = MapQueryfier(urlFavs: {marmotaUrl});
      await favQueryfier.marcarFavorito(marmotaUrl, false);
      var favoritou = await favQueryfier.favorito(marmotaUrl);
      expect(favoritou, equals(false));
    });
    test("pseudobanco reconhece toggle sobre URL da construção", () async {
      var favQueryfier = MapQueryfier(urlFavs: {marmotaUrl});
      await favQueryfier.toggleFavorito(marmotaUrl);
      var favoritou = await favQueryfier.favorito(marmotaUrl);
      expect(favoritou, equals(false));
    });
  });
  group("testando persistir notícias no pseudobanco", () {
    test("reconhecer notícia como não favorita", () async {
      var favQueryfier = MapQueryfier();
      var noticia = Noticia(url: marmotaUrl, queryfier: favQueryfier);
      expect(await noticia.fav, equals(false));
    });
    test("reconhecer notícia como favorita", () async {
      var favQueryfier = MapQueryfier(urlFavs: {marmotaUrl});
      var noticia = Noticia(url: marmotaUrl, queryfier: favQueryfier);
      expect(await noticia.fav, equals(true));
    });
    test("salvar notícia como favorita", () async {
      var favQueryfier = MapQueryfier();
      var noticia = Noticia(url: marmotaUrl, queryfier: favQueryfier);
      await noticia.alterarStatusFav();
      expect(await noticia.fav, equals(true));
    });
    test("remover notícia como favorita", () async {
      var favQueryfier = MapQueryfier(urlFavs: {marmotaUrl});
      var noticia = Noticia(url: marmotaUrl, queryfier: favQueryfier);
      await noticia.alterarStatusFav();
      expect(await noticia.fav, equals(false));
    });
  });
  group("testando transformar json em notícia", () {
    test("consigo gerar uma notícia", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      Map<String, dynamic> jsonNoticia = { 'url': marmotaUrl };
      var noticiaParseada = noticiaFactory.jsonToNoticia(jsonNoticia);
      expect(noticiaParseada.url, equals(marmotaUrl));
    });
    test("consigo gerar duas notícias", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      Map<String, dynamic> jsonNoticia1 = { 'url': marmotaUrl };
      Map<String, dynamic> jsonNoticia2 = { 'url': outraUrl };
      var noticiaParseada1 = noticiaFactory.jsonToNoticia(jsonNoticia1);
      var noticiaParseada2 = noticiaFactory.jsonToNoticia(jsonNoticia2);
      expect(noticiaParseada1.url, equals(marmotaUrl));
      expect(noticiaParseada2.url, equals(outraUrl));
    });
    test("consigo gerar uma notícia numa lista", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      Map<String, dynamic> jsonNoticia = { 'url': marmotaUrl };
      List<dynamic> jsonNoticias = [jsonNoticia];
      var noticiasParseadas = noticiaFactory.jsonListToNoticia(jsonNoticias);
      expect(noticiasParseadas.length, equals(1));
      expect(noticiasParseadas[0].url, equals(marmotaUrl));
    });
    test("consigo gerar duas notícias numa lista", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      Map<String, dynamic> jsonNoticia1 = { 'url': marmotaUrl };
      Map<String, dynamic> jsonNoticia2 = { 'url': outraUrl };
      List<dynamic> jsonNoticias = [jsonNoticia1, jsonNoticia2];
      var noticiasParseadas = noticiaFactory.jsonListToNoticia(jsonNoticias);
      expect(noticiasParseadas.length, equals(2));
      expect(noticiasParseadas[0].url, equals(marmotaUrl));
      expect(noticiasParseadas[1].url, equals(outraUrl));
    });
    test("url explicitada nula deve estourar", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      Map<String, dynamic> jsonNoticia = { 'url': null };
      expect(() => noticiaFactory.jsonToNoticia(jsonNoticia), throwsA(isA<Error>()));
    });
    test("url implícita nula deve estourar", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      Map<String, dynamic> jsonNoticia = { };
      expect(() => noticiaFactory.jsonToNoticia(jsonNoticia), throwsA(isA<Error>()));
    });
    test("consigo gerar duas notícias e marcar como favorita", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      Map<String, dynamic> jsonNoticia1 = { 'url': marmotaUrl };
      Map<String, dynamic> jsonNoticia2 = { 'url': outraUrl };
      var noticiaParseada1 = noticiaFactory.jsonToNoticia(jsonNoticia1);
      var noticiaParseada2 = noticiaFactory.jsonToNoticia(jsonNoticia2);

      await noticiaParseada1.alterarStatusFav();
      expect(noticiaParseada1.url, equals(marmotaUrl));
      expect(noticiaParseada2.url, equals(outraUrl));
      expect(await noticiaParseada1.fav, equals(true));
    });
    test("consigo serializar todos os capos da notícia", () async {
      var favQueryfier = MapQueryfier();
      var noticiaFactory = NoticiaFactory(queryfier: favQueryfier);
      final title = "News 1 title";
      final description = "News 1 description";
      final content = "News 1 content";
      final author = "News 1 author";
      final publishedAt = "2020-01-30T13:45:00.000Z";
      final highlight = false;
      final url = "https://fake.news";
      final imageUrl = "https://via.placeholder.com/600x300";
      Map<String, dynamic> jsonNoticia = {
        "title": title,
        "description": description,
        "content": content,
        "author": author,
        "published_at": publishedAt,
        "highlight": highlight,
        "url": url,
        "image_url": imageUrl
      };
      var noticiaParseada = noticiaFactory.jsonToNoticia(jsonNoticia);
      expect(noticiaParseada.url, equals(url));
      expect(noticiaParseada.author, equals(author));
      expect(noticiaParseada.content, equals(content));
      expect(noticiaParseada.description, equals(description));
      expect(noticiaParseada.imageUrl, equals(imageUrl));
      expect(noticiaParseada.publishedAt, equals(publishedAt));
      expect(noticiaParseada.title, equals(title));
    });
  });
}