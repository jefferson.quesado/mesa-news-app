import 'package:mesa_news/model/noticia.dart';

class MapQueryfier extends NoticiaQueryfier {
  final Set<String> _urlsFavs;

  MapQueryfier({ Set<String>? urlFavs }): this._urlsFavs = urlFavs ?? Set<String>();

  @override
  Future<bool> favorito(String url) async {
    return _urlsFavs.contains(url);
  }

  @override
  Future<bool> marcarFavorito(String url, bool fav) async {
    if (fav) {
      _urlsFavs.add(url);
    } else {
      _urlsFavs.remove(url);
    }
    return true;
  }
}