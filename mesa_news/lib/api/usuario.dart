import 'dart:convert';

import 'package:http/http.dart';
import 'package:mesa_news/api/utils.dart';

Future<Response> login(String email, String senha) async {
  Map<String, String> m = {
    'email': email,
    'password': senha
  };
  return await post(
    Uri.parse(apiMesaBaseUrl + 'v1/client/auth/signin'),
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode(m)
  );
}

Future<Response> criarUsuario(String nome, String email, String senha) async {
  Map<String, String> m = {
    'name': nome,
    'email': email,
    'password': senha
  };
  return await post(
      Uri.parse(apiMesaBaseUrl + 'v1/client/auth/signup'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(m)
  );
}