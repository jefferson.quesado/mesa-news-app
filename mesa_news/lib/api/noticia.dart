import 'package:http/http.dart';
import 'package:mesa_news/api/utils.dart';
import 'package:mesa_news/model/usuario.dart';

Future<Response> listagem({int? currentPage, int? perPage, String? publicadoEm}) async {
  if (!existeUsuarioLogado()) {
    throw Exception('Precisa estar logado para perguntar resgatar notícias do serviço');
  }
  Uri semQuery = Uri.parse(apiMesaBaseUrl + 'v1/client/news');
  Uri definitiva;

  Map<String, dynamic> params = {};
  if (currentPage != null) {
    params['current_page'] = '$currentPage';
  }
  if (perPage != null) {
    params['per_page'] = '$perPage';
  }
  if (publicadoEm != null) {
    params['published_at'] = publicadoEm;
  }
  if (params.isEmpty) {
    definitiva = semQuery;
  } else {
    if (semQuery.isScheme('https')) {
      definitiva = Uri.https(semQuery.authority, semQuery.path, params);
    } else {
      definitiva = Uri.http(semQuery.authority, semQuery.path, params);
    }
  }
  return await get(definitiva, headers: {
    'Authorization': 'Bearer ${usuarioLogado!.token}',
  });
}

Future<Response> destaques() async {
  if (!existeUsuarioLogado()) {
    throw Exception('Precisa estar logado para perguntar resgatar notícias do serviço');
  }

  return await get(Uri.parse(apiMesaBaseUrl + 'v1/client/news/hightlights'), headers: {
    'Authorization': 'Bearer ${usuarioLogado!.token}',
  });
}