abstract class NoticiaQueryfier {
  Future<bool> favorito(String url);
  Future<bool> marcarFavorito(String url, bool fav);

  Future<bool> toggleFavorito(String url) async => await marcarFavorito(url, !await favorito(url));
}

class Noticia {

  final String url;
  final String? imageUrl;

  final String? title;
  final String? description;
  final String? content;
  final String? author;
  final String? publishedAt;

  final NoticiaQueryfier _queryfier;

  Future<bool> get fav async => await _queryfier.favorito(url);

  Noticia({
    required this.url,
    required NoticiaQueryfier queryfier,
    this.imageUrl,
    this.title,
    this.description,
    this.content,
    this.author,
    this.publishedAt
  }): this._queryfier = queryfier;

  String get domain => Uri.parse(url).host;

  Future<bool> alterarStatusFav() async => await _queryfier.toggleFavorito(url);
}

class NoticiaFactory {

  final NoticiaQueryfier _queryfier;

  NoticiaFactory({ required NoticiaQueryfier queryfier }): this._queryfier = queryfier;

  Noticia jsonToNoticia(Map<String, dynamic> jsonNoticia) =>
      Noticia(url: jsonNoticia['url'],
          queryfier: _queryfier,
          title: jsonNoticia["title"],
          description: jsonNoticia["description"],
          content: jsonNoticia["content"],
          author: jsonNoticia["author"],
          publishedAt: jsonNoticia["published_at"],
          imageUrl: jsonNoticia["image_url"]);

  List<Noticia> jsonListToNoticia(List<dynamic> jsonNoticias) =>
      jsonNoticias.map((i) => jsonToNoticia(i as Map<String, dynamic>)).toList();
}