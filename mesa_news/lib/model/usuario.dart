abstract class UsuarioQueryfier {
  Future<int> resgataCodigoUsuarioSalvandoSeNovo(Usuario u);
}

class Usuario {

  int? codigo;
  final String email;
  String token;

  Usuario(this.email, this.token);
}

Usuario? _usuarioLogado;

Usuario? get usuarioLogado => _usuarioLogado;
bool existeUsuarioLogado() => _usuarioLogado != null;
set usuarioLogado(Usuario? usuario) => _usuarioLogado = usuario;