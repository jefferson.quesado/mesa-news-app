import 'package:flutter/material.dart';
import 'package:mesa_news/database/db.dart';
import 'package:mesa_news/view/criar_usuario.dart';
import 'package:mesa_news/view/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mesa News',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Mesa News'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key) {
    _initDB();
  }

  Future<void> _initDB() async {
    await databaseAtual;
  }

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(image: AssetImage('assets/logo-mesa.png')),
                    Text(
                        'NEWS',
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontFamily: 'Roboto',
                          fontSize: 19,
                          letterSpacing: 25,
                        ),
                    ),
                  ]
                )
              )
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                children: [
                  ElevatedButton(
                    child: Text('Entrar com email'),
                    onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return Login();
                    })),
                  ),
                  TextButton(
                    child: Text('Criar novo usuário'),
                    onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return CriarUsuario();
                    })),
                  ),
                ]
              )
            ),
          ],
        ),
      ),
    );
  }
}
