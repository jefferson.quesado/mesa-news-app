import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mesa_news/model/noticia.dart';
import 'package:mesa_news/view/webview_noticia.dart';

class FragNoticia extends StatefulWidget {

  final Noticia noticia;
  FragNoticia({required this.noticia});

  @override
  _FragNoticiaState createState() => _FragNoticiaState(noticia);
}

class _FragNoticiaState extends State<FragNoticia> {

  final Noticia noticia;
  bool _iniciado = false;
  bool _favoritado = false;
  _FragNoticiaState(Noticia noticia): this.noticia = noticia;

  @override
  void initState() {
    super.initState();
    _iniciaFavoritos();
  }

  Future<void> _iniciaFavoritos() async {
    bool favoritado = await noticia.fav;
    setState(() {
      _favoritado = favoritado;
      _iniciado = true;
    });
  }

  Future<void> toggleFav() async {
    bool atualizou = await noticia.alterarStatusFav();
    if (atualizou) {
      bool fav = await noticia.fav;
      setState(() {
        this._favoritado = fav;
      });
    } // else mostra falha de alteração de status
  }

  void _abrirNoticia(BuildContext context) async {
    await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) {
          return WebviewNoticia(noticia, _favoritado);
        })
    );
    bool fav = await noticia.fav;
    if (fav != _favoritado) {
      setState(() {
        _favoritado = fav;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child:
        InkWell(
            child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (noticia.imageUrl != null) CachedNetworkImage(imageUrl: noticia.imageUrl!),
              Row(
                children: [
                  IconButton(icon: Icon(_favoritado ? Icons.bookmark : Icons.bookmark_border), onPressed: () {
                    if (!_iniciado) return;
                    toggleFav();
                  }),
                  Expanded(child: Container()),
                  Text(noticia.publishedAt ?? 'sem data',
                      style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ],
              ),
              if (noticia.title != null)
                Text(
                    noticia.title!,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
              if (noticia.description != null)
                Text(noticia.description!),
            ]
          ),
          onTap: () => _abrirNoticia(context),
        )
    );
  }
}