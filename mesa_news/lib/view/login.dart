import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mesa_news/api/usuario.dart';
import 'package:mesa_news/database/queryfiers.dart';
import 'package:mesa_news/model/usuario.dart';
import 'package:mesa_news/view/feed_noticia.dart';

class _Placeholder {

  String email = '';
  String senha = '';
}

class Login extends StatelessWidget {

  final _Placeholder _placeholder = _Placeholder();

  Future<void> _lidarResposta(Response r, BuildContext context) async {
    if (r.statusCode == 200) {
      String token = jsonDecode(r.body)['token'] as String;
      Usuario u = Usuario(_placeholder.email, token);
      int x = await UsuarioSqlite().resgataCodigoUsuarioSalvandoSeNovo(u);
      u.codigo = x;
      usuarioLogado = u;
      while (Navigator.canPop(context)) {
        Navigator.pop(context);
      }
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => new FeedNoticias())
      );
    } else if (r.statusCode == 401) {
      // toast de credenciais inválidas
    } else if (r.statusCode >= 400 && r.statusCode < 500) {
      // toast de erro de requisição
    } else if (r.statusCode >= 500) {
      // toast de erro do servidor
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Entrar com email')
        )
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(image: AssetImage('assets/fancy-login.png')),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Email'),
                TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder()
                  ),
                  onChanged: (String s) => _placeholder.email = s
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Senha'),
                TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder()
                  ),
                  obscureText: true,
                  onChanged: (String s) => _placeholder.senha = s
                ),
              ],
            ),
            ElevatedButton(
              child: Text('Login'),
              onPressed: () async => _lidarResposta(await login(_placeholder.email, _placeholder.senha), context)
            )
          ],
        )
      )
    );
  }
}