import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mesa_news/api/noticia.dart';
import 'package:mesa_news/database/queryfiers.dart';
import 'package:mesa_news/model/noticia.dart';
import 'package:mesa_news/view/frag_noticia.dart';

class FeedNoticias extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _FeedNoticiasState(NoticiaFactory(queryfier: NoticiaSqlite()));
}

class _FeedNoticiasState extends State<FeedNoticias> {

  List<Noticia> _noticias = [];
  int _total = -1;
  int _perPage = -1;
  int _paginaAtual = -1;
  bool _error = false;

  NoticiaFactory noticiaFactory;
  _FeedNoticiasState(this.noticiaFactory) {
    primeiraConsulta();
  }

  Future<List<Noticia>> proximo() async {
    _error = false;
    if (_paginaAtual == -1) {
      return primeiraConsulta();
    } else {
      return consultasIncrementais();
    }
  }

  Future<List<Noticia>> consultasIncrementais() async {
    Response resp = await listagem(perPage: _perPage, currentPage: _paginaAtual + 1);
    if (resp.statusCode == 200) {
      Map<String, dynamic> fetched = jsonDecode(resp.body);
      List<dynamic> noticiasJson = fetched['data'];
      List<Noticia> resgatadas = noticiaFactory.jsonListToNoticia(noticiasJson);

      setState(() {
        _paginaAtual++;
        _noticias.addAll(resgatadas);
      });
      return resgatadas;
    } else {
      _error = true;
      return [];
    }
  }

  Future<List<Noticia>> primeiraConsulta() async {
    Response resp = await listagem();
    if (resp.statusCode == 200) {
      Map<String, dynamic> fetched = jsonDecode(resp.body);
      Map<String, dynamic> paginacao = fetched['pagination'];

      List<dynamic> noticiasJson = fetched['data'];
      List<Noticia> resgatadas = noticiaFactory.jsonListToNoticia(noticiasJson);

      setState(() {
        _total = paginacao['total_items'];
        _perPage = paginacao['per_page'];
        _paginaAtual = 1;
        _noticias.addAll(resgatadas);
      });
      return resgatadas;
    } else {
      _error = true;
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Mesa News')
        )
      ),
      body: getBody(context),
    );
  }

  Widget getBody(BuildContext context) {
    if (_paginaAtual == -1 && _error) {
      return InkWell(
          child: Text('Deu falha, clique para tentar de novo...'),
          onTap: () {
            proximo();
          }
      );
    } else if (_paginaAtual == -1) {
      proximo();
      return InkWell(
        child: Text('Tentando pegar de novo, clique para insistir...'),
        onTap: () {
          proximo();
        }
      );
    } else {
      return ListView.builder(
        itemCount: min(_total, _noticias.length + 1),
        itemBuilder: (context, index) {
          if (index >= _total) {
            return Text('FIm da leitura =D');
          }
          if (index >= _noticias.length) {
            proximo();
            return InkWell(
              child: Text('Tentando pegar o próximo, clique para insistir...'),
              onTap: () {
                proximo();
              }
            );
          }
          return FragNoticia(noticia: _noticias[index]);
        },
      );
    }
  }
}