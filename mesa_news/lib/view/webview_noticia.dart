import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mesa_news/model/noticia.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewNoticia extends StatefulWidget {

  final Noticia noticia;
  final bool favInit;

  WebviewNoticia(Noticia noticia, bool favInit): this.noticia = noticia, this.favInit = favInit;

  @override
  _WebviewNoticiaState createState() => _WebviewNoticiaState(noticia, favInit);
}

class _WebviewNoticiaState extends State<WebviewNoticia> {

  final Noticia noticia;
  bool fav;

  _WebviewNoticiaState(Noticia noticia, bool favInit): this.noticia = noticia, this.fav = favInit;

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  Future<void> _toggleFav() async {
    if (await noticia.alterarStatusFav()) {
      setState(() {
        fav = !fav;
      });
    } else {
      // caso em que deu falha no salvar
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Center(
          child: Column(
            children: [
              if (noticia.title != null) Text(noticia.title!, textAlign: TextAlign.center,),
              Text(noticia.domain, style: TextStyle(fontSize: 15), textAlign: TextAlign.center,),
            ]
          ),
        ),
      ),
      body: Column(
        children: [
          if (noticia.imageUrl != null) CachedNetworkImage(imageUrl: noticia.imageUrl!),
          Row(
            children: [
              IconButton(icon: Icon(fav ? Icons.bookmark : Icons.bookmark_border), onPressed: _toggleFav),
              Expanded(child: Container()),
              Text(noticia.publishedAt ?? 'sem data',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ],
          ),
          Expanded(
            child: WebView(initialUrl: noticia.url)
          ),
        ],
      ),
    );
  }
}