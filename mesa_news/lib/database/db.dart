import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

Completer<Database> _data = Completer<Database>();
Future<Database> get databaseAtual async {
  if (_data.isCompleted) {
    return _data.future;
  }
  var db = await openDatabase('path', version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade);
  _data.complete(db);
  return _data.future;
}

Future<void> _onCreate(Database db, int v) async {
  await db.execute('''
    CREATE TABLE usuario (
      cd_usuario INTEGER PRIMARY KEY,
      email STRING NOT NULL
    )
  ''');
  await db.execute('''
    CREATE TABLE noticia (
      cd_usuario INTEGER NOT NULL,
      url STRING NOT NULL,
      title STRING,
      description STRING,
      content STRING,
      author STRING,
      published_at STRING,
      PRIMARY KEY (cd_usuario, url)
    )
  ''');
}

Future<void> _onUpgrade(Database db, int oldV, int newV) async {

}