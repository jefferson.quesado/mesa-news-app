import 'package:mesa_news/database/db.dart';
import 'package:mesa_news/model/noticia.dart';
import 'package:mesa_news/model/usuario.dart';
import 'package:sqflite/sqflite.dart';

class _UserDb {
  Usuario user;
  Database db;

  _UserDb(this.user, this.db);
}

class NoticiaSqlite extends NoticiaQueryfier {

  Future<_UserDb> _bancoComUsuario() async {
    if (!existeUsuarioLogado()) {
      throw Exception('Precisa estar logado para perguntar se a notícia é favorita');
    }
    var db = await databaseAtual;
    return _UserDb(usuarioLogado!, db);
  }

  @override
  Future<bool> favorito(String url) async {
    var udb = await _bancoComUsuario();
    var x = await udb.db.rawQuery('SELECT 1 FROM noticia WHERE cd_usuario = ? AND url = ?', [ udb.user.codigo, url ]);
    return x.isNotEmpty;
  }

  @override
  Future<bool> marcarFavorito(String url, bool fav) async {
    var udb = await _bancoComUsuario();
    if (fav) {
      var q = await udb.db.rawUpdate(
        '''
          INSERT INTO noticia (cd_usuario, url)
          SELECT ? AS cd_usuario, ? as url
          WHERE NOT EXISTS (SELECT 1 FROM noticia n WHERE n.cd_usuario = ? AND n.url = ?)
        ''',
        [ udb.user.codigo, url, udb.user.codigo, url ]
      );
      return q != 0;
    } else {
      var q = await udb.db.rawDelete('DELETE FROM noticia WHERE cd_usuario = ? AND url = ?', [ udb.user.codigo, url ]);
      return q != 0;
    }
  }
}

class UsuarioSqlite extends UsuarioQueryfier {

  @override
  Future<int> resgataCodigoUsuarioSalvandoSeNovo(Usuario u) async {
    var db = await databaseAtual;
    var x = await db.rawQuery('SELECT cd_usuario FROM usuario WHERE email = ?', [ u.email ]);
    if (x.isNotEmpty) {
      return x.first['cd_usuario'] as int;
    }
    return await db.rawInsert('INSERT INTO usuario (email) VALUES (?)', [ u.email ]);
  }
}